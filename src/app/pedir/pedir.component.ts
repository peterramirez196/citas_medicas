import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {VehiculoService} from '../servicios/vehiculo.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-pedir',
  templateUrl: './pedir.component.html',
  styleUrls: ['./pedir.component.css']
})
export class PedirComponent implements OnInit {
  objeto: any = { cedula: '', fecha: '', hora: '', seguro: ''};
  objeto1: any = { cedula: '', fecha: '', hora: '', seguro: ''};
  constructor(private servicio: VehiculoService , private router: Router) {
    this.objeto.cedula = localStorage.getItem('user');
    console.log(this.objeto.cedula);
  }

  ngOnInit() {
  }
  login( form: NgForm) {
    console.log(form.value);
    this.objeto1 = form.value;
    this.guardar();
  }
  guardar() {
    this.servicio.postVehiculo(this.objeto1).subscribe(resultado => {
      console.log(resultado);
      console.log(this.objeto1);
      alert('Guarda con exito');
      this.router.navigate(['citas']);
    });
  }
}
