import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {LoginService} from './servicios/login.service';
import {PruebaService} from './servicios/prueba.service';
import {MatriculaService} from './servicios/matricula.service';
import { Route } from '@angular/compiler/src/core';
import {RouterModule, Routes} from '@angular/router';
import {LogiGuard} from './logi.guard';

import { CitasmedicasComponent } from './citasmedicas/citasmedicas.component';
import { ContactosComponent } from './contactos/contactos.component';
import { HistorialComponent } from './historial/historial.component';
import { PedirComponent } from './pedir/pedir.component';
import { SerComponent } from './ser/ser.component';



const rutas: Routes = [

  { path: 'principal', component: AppComponent},
  {path: 'citas', component: CitasmedicasComponent},
  {path: 'contactos', component: ContactosComponent},
  {path: 'historial', component: HistorialComponent},
  {path: 'pedir', component: PedirComponent},
  {path: 'servicios', component: SerComponent},


];

@NgModule({
  declarations: [
    AppComponent,
    CitasmedicasComponent,

    ContactosComponent,
    HistorialComponent,
    PedirComponent,
    SerComponent,
  ],
  imports: [

    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(rutas),
  ],
  providers: [LoginService, LogiGuard, PruebaService,MatriculaService],
  bootstrap: [AppComponent]
})
export class AppModule {

}
