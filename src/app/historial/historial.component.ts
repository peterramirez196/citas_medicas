import { Component, OnInit } from '@angular/core';
import {LoginService} from '../servicios/login.service';
import {NgForm} from '@angular/forms';
import {VehiculoService} from '../servicios/vehiculo.service';



@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.css']
})
export class HistorialComponent implements OnInit {


  objetoVehiculo: any = { cedula: '', contrasena: '', correo: '', nombre: '', direccion: '', celular: '', tipo: ''};
  objeto: any = { cedula: '', contrasena: '', correo: '', nombre: '', direccion: '', celular: '', tipo: ''};
   objeto1: any = { cedula: '', fecha: '', hora: '', seguro: ''};
  objeto15: any = { cedula: '', fecha: '', hora: '', seguro: ''};
  objeto5: any = { cedula: '', fecha: '', hora: '', seguro: ''};

  constructor(private servicio: LoginService, private servicio1: VehiculoService) {
    this.objeto.cedula = localStorage.getItem('user');
    console.log(this.objeto.cedula);
    this.consultaIndividual();
    this.consultarma();
  }

  ngOnInit() {
  }
  login( form: NgForm) {
    console.log(form.value);
    this.objetoVehiculo = form.value;
    this.objetoVehiculo.tipo = 'user';
    this.guardar();
  }
  guardar() {
    this.servicio.postVehiculo(this.objetoVehiculo).subscribe(resultado => {
      console.log(resultado);
      console.log(this.objetoVehiculo);

      alert('Guarda con exito');
    });
  }

  consultaIndividual() {
    this.servicio.getuser(this.objeto.cedula).subscribe(resultado => {
      this.objeto = resultado;
      console.log(this.objeto);
    });
  }
  consultarma() {
    this.servicio1.getVehiculos().subscribe(datos => {
      this.objeto1 = datos;
      console.log(this.objeto1);
    });
  }
  consultaIndividual1(placax1: string) {
    this.servicio1.getVehiculo(placax1).subscribe(resultado => {

      this.objeto5 = resultado;
      console.log(this.objeto5);
    });
  }
}
