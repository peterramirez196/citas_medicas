import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PruebaService {
  url:any ='http://localhost:3000/vehiculos';
  constructor(private http:HttpClient) { }

  getusers(){

    return this.http.get(this.url,
      {headers:{"Content-Type":"application/json"}}
    )
  }
  getuser(placax){
    return this.http.get(this.url+'/'+placax,
      {headers:{'Content-Type':'application/json'}})
  }
  postVehiculo(vehiculox){
    return this.http.post(this.url, vehiculox,
      {headers:{"Content-Type":"application/json"},responseType:'text'})
  }

  deleteVehiculo(placax){
    return this.http.delete(this.url+'/'+placax,
      {headers:{"Content-Type":"application/json"},responseType:'text'})
  }
}
