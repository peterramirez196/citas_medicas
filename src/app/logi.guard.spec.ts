import { TestBed, async, inject } from '@angular/core/testing';

import { LogiGuard } from './logi.guard';

describe('LogiGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LogiGuard]
    });
  });

  it('should ...', inject([LogiGuard], (guard: LogiGuard) => {
    expect(guard).toBeTruthy();
  }));
});
